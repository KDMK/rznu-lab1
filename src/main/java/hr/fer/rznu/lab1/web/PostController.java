package hr.fer.rznu.lab1.web;

import static hr.fer.rznu.lab1.security.AuthenticationRoles.ADMIN;
import hr.fer.rznu.lab1.service.PostService;
import hr.fer.rznu.lab1.web.dto.CreatePostDto;
import hr.fer.rznu.lab1.web.dto.UpdatePostDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import javax.ws.rs.core.Response;

@Path("/post")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@PermitAll
@Api("Post service")
public class PostController {

    @Inject
    private PostService postService;

    @GET
    @ApiOperation(value = "Fetch all posts", httpMethod = "GET")
    public Response getAllPosts() {
        return Response.ok(postService.getAll()).build();
    }

    @GET
    @Path("/{id}")
    @ApiOperation(value = "Get one post", httpMethod = "GET")
    public Response getOne(@PathParam("id") Long id) {
        return Response.ok(postService.getPost(id)).build();
    }

    @GET
    @Path("/{id}/user")
    @ApiOperation(value = "Get post user", httpMethod = "GET")
    public Response getPostUser(@PathParam("id") Long id) {
        var post = postService.getPost(id);
        return Response.ok(post.getCreatedBy()).build();
    }

    @POST
    @ApiOperation(value = "Create new post", httpMethod = "POST")
    public Response createPost(CreatePostDto createPostDto) {
        return Response.status(201).entity(postService.createNewPost(createPostDto)).build();
    }

    @PATCH
    @Path("{id}")
    @ApiOperation(value = "Update existing post", httpMethod = "PATCH")
    public Response updatePost(@PathParam("id") Long postId, UpdatePostDto updatePostDto) {
        return Response.ok(postService.updatePost(postId, updatePostDto)).build();
    }

    @DELETE
    @Path("{id}")
    @RolesAllowed({ ADMIN })
    @ApiOperation(value = "Delete post", httpMethod = "DELETE")
    public Response deletePost(@PathParam("id") Long postId) {
        postService.deletePost(postId);
        return Response.ok().build();
    }

}
