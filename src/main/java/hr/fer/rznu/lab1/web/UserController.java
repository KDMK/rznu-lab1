package hr.fer.rznu.lab1.web;

import hr.fer.rznu.lab1.model.User;
import static hr.fer.rznu.lab1.security.AuthenticationRoles.ADMIN;
import static hr.fer.rznu.lab1.security.AuthenticationRoles.USER;
import hr.fer.rznu.lab1.service.PostService;
import hr.fer.rznu.lab1.service.UserService;
import hr.fer.rznu.lab1.web.dto.CreateUserDto;
import hr.fer.rznu.lab1.web.dto.UpdateUserDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Stateful;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.Status.CREATED;
import static javax.ws.rs.core.Response.Status.NO_CONTENT;
import static javax.ws.rs.core.Response.Status.OK;
import static org.apache.logging.log4j.LogManager.getLogger;
import org.apache.logging.log4j.Logger;

@RequestScoped
@Stateful
@Path("/user")
@Api(value = "User service")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
public class UserController {

    private static final Logger LOG = getLogger(UserController.class);

    @Inject
    private UserService userService;
    @Inject
    private PostService postService;

    @GET
    @ApiOperation(value = "Get all users", httpMethod = "GET")
    @RolesAllowed({ ADMIN, USER })
    public Response getAll(@Context HttpHeaders headers) {
        LOG.info("{} -> getAllUsers", headers.getHeaderString(HttpHeaders.HOST));
        List<User> users = userService.getAllUsers();
        return Response
                .status(users.isEmpty() ? NO_CONTENT : OK)
                .entity(users)
                .build();
    }

    @GET
    @Path("{id}")
    @ApiOperation(value = "Get user by id", httpMethod = "GET")
    @RolesAllowed({ ADMIN, USER })
    public Response getUser(@PathParam("id") Long id) {
        User user = userService.getOne(id);
        return Response.ok(user).build();
    }

    @GET
    @Path("/username/{username}")
    @RolesAllowed({ ADMIN, USER })
    @ApiOperation(value = "Get user by username", httpMethod = "GET")
    public Response getUserByUsername(@PathParam("username") String username) {
        User user = userService.getOne(username);
        return Response.ok(user).build();
    }

    @GET
    @Path("/email/{email}")
    @RolesAllowed({ ADMIN, USER })
    @ApiOperation(value = "Get user by email", httpMethod = "GET")
    public Response getUserByEmail(@PathParam("email") String email) {
        User user = userService.getOneByEmail(email);
        return Response.ok(user).build();
    }

    @POST
    @ApiOperation(value = "Create new user", httpMethod = "POST")
    @RolesAllowed({ ADMIN })
    public Response createUser(CreateUserDto createUserDto, @Context HttpHeaders headers) {
        LOG.info("{} -> createUser", headers.getHeaderString(HttpHeaders.HOST));
        var createdUser = userService.createUser(createUserDto);
        return Response.status(CREATED).entity(createdUser).build();
    }

    @PUT
    @ApiOperation(value = "Update existing user", httpMethod = "PUT")
    @RolesAllowed({ ADMIN })
    public Response updateUser(UpdateUserDto updateUserDto, @Context HttpHeaders headers) {
        LOG.info("{} -> updateUser", headers.getHeaderString(HttpHeaders.HOST));
        var updateUser = userService.updateUser(updateUserDto);
        return Response.accepted(updateUser).build();
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(value = "Deletes existing user", httpMethod = "DELETE")
    @RolesAllowed({ ADMIN })
    public Response deleteUser(@PathParam("id") Long id) {
        userService.delete(id);
        return Response.ok().build();
    }

    @POST
    @Path("{id}/promote")
    @RolesAllowed({ ADMIN })
    @ApiOperation(value = "Promotes existing user to admin user.", httpMethod = "POST")
    public Response promoteUser(@PathParam("id") Long id) {
        userService.addRoleToUser(id, ADMIN);
        return Response.ok().build();
    }

    @POST
    @Path("{id}/revoke")
    @RolesAllowed({ ADMIN })
    @ApiOperation(value = "Revokes admin rights from user.", httpMethod = "POST")
    public Response downgradeUser(@PathParam("id") Long id) {
        userService.removeRoleFromUser(id, ADMIN);
        return Response.ok().build();
    }

    @GET
    @Path("{id}/posts")
    @RolesAllowed({ USER, ADMIN })
    @ApiOperation(value = "Fetches all posts by user.", httpMethod = "GET")
    public Response getPostsByUser(@PathParam("id") Long userId) {
        return Response.ok(postService.getPostsByUser(userId)).build();
    }

}
