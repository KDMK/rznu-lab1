package hr.fer.rznu.lab1.web.dto;

public class CreateTopicDto {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
