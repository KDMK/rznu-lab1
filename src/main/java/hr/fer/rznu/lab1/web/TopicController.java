package hr.fer.rznu.lab1.web;

import hr.fer.rznu.lab1.service.PostService;
import hr.fer.rznu.lab1.service.TopicService;
import hr.fer.rznu.lab1.web.dto.CreateTopicDto;
import hr.fer.rznu.lab1.web.dto.UpdateTopicDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import javax.ws.rs.core.Response;

@Path("/topic")
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Api(value = "Topic service")
public class TopicController {

    @Inject
    private TopicService topicService;
    @Inject
    private PostService  postService;

    @GET
    @ApiOperation(value = "Get all topics", httpMethod = "GET")
    public Response getAllTopics() {
        return Response.ok(topicService.getAllTopics()).build();
    }

    @GET
    @Path("/{id}")
    @ApiOperation(value = "Get topic with id", httpMethod = "GET")
    public Response getOne(@PathParam("id") Long id) {
        return Response.ok(topicService.getTopic(id)).build();
    }

    @GET
    @Path("/{topicId}/posts")
    @ApiOperation(value = "Get all posts by topic", httpMethod = "GET")
    public Response getByTopic(@PathParam("topicId") Long topicId) {
        return Response.ok(postService.getPostsByTopic(topicId)).build();
    }

    @POST
    @ApiOperation(value = "Create new post", httpMethod = "POST")
    public Response createPost(CreateTopicDto createTopicDto) {
        return Response.status(201).entity(topicService.createTopic(createTopicDto)).build();
    }

    @PATCH
    @Path("{id}")
    @ApiOperation(value = "Update existing post", httpMethod = "PATCH")
    public Response updatePost(@PathParam("id") Long topicId, UpdateTopicDto updateTopicDto) {
        return Response.ok(topicService.updateTopic(topicId, updateTopicDto)).build();
    }

    @DELETE
    @Path("{id}")
    @ApiOperation(value = "Delete post", httpMethod = "DELETE")
    public Response deletePost(@PathParam("id") Long topicId) {
        topicService.deleteTopic(topicId);
        return Response.ok().build();
    }
}
