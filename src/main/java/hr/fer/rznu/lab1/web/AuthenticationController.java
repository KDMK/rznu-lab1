package hr.fer.rznu.lab1.web;

import hr.fer.rznu.lab1.security.TokenProvider;
import hr.fer.rznu.lab1.web.dto.LoginUserDto;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.security.Principal;
import javax.annotation.security.PermitAll;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.security.enterprise.SecurityContext;
import javax.security.enterprise.authentication.mechanism.http.HttpMessageContext;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import static javax.security.enterprise.identitystore.CredentialValidationResult.Status.VALID;
import javax.security.enterprise.identitystore.IdentityStoreHandler;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import static org.apache.logging.log4j.LogManager.getLogger;
import org.apache.logging.log4j.Logger;

@Path("auth")
@Api("Authentication API")
public class AuthenticationController {

    private static final Logger LOG = getLogger(AuthenticationController.class);

    @Inject
    private SecurityContext      context;
    @Inject
    private IdentityStoreHandler identityStoreHandler;
    @Inject
    private TokenProvider        tokenProvider;

    @POST
    @Path("login")
    @ApiOperation(value = "Perform user login", httpMethod = "POST")
    @PermitAll
    public Response login(@Context HttpServletRequest servletRequest, @Context HttpMessageContext messageContext,
            LoginUserDto body) {
        LOG.info("login");
        Principal principal = context.getCallerPrincipal();
        // User provided token, and token is valid - do nothing except notify user that he is logged in
        if (principal != null) {
            JsonObject result = Json.createObjectBuilder()
                    .add("user", principal.getName())
                    .build();
            return Response.ok(result).build();
        }

        // User didn't provide token, try to create token and login user
        CredentialValidationResult validationResult = validateCredentials(body);
        if (validationResult.getStatus() == VALID) {
            return Response.status(200).entity(createToken(validationResult, messageContext)).build();
        }

        return Response.status(UNAUTHORIZED).build();
    }

    private CredentialValidationResult validateCredentials(LoginUserDto body) {
        UsernamePasswordCredential credential = new UsernamePasswordCredential(body.getUsername(), body.getPassword());
        return identityStoreHandler.validate(credential);
    }

    private String createToken(CredentialValidationResult result, HttpMessageContext context) {
        String callerName = result.getCallerPrincipal().getName();
        return tokenProvider.createToken(callerName, result.getCallerGroups());
    }

}
