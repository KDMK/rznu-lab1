package hr.fer.rznu.lab1.security;

import io.jsonwebtoken.ExpiredJwtException;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.security.enterprise.AuthenticationException;
import javax.security.enterprise.AuthenticationStatus;
import javax.security.enterprise.authentication.mechanism.http.HttpAuthenticationMechanism;
import javax.security.enterprise.authentication.mechanism.http.HttpMessageContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.apache.logging.log4j.LogManager.getLogger;
import org.apache.logging.log4j.Logger;

@RequestScoped
public class JWTAuthenticationMechanism implements HttpAuthenticationMechanism {

    private static final Logger LOG = getLogger(JWTAuthenticationMechanism.class);

    @Inject
    private TokenProvider tokenProvider;

    @Override
    public AuthenticationStatus validateRequest(HttpServletRequest request, HttpServletResponse response,
            HttpMessageContext httpMessageContext) throws AuthenticationException {
        var token = extractToken(httpMessageContext);

        if (token != null) {
            return validateToken(token, httpMessageContext);
        } else if (httpMessageContext.isProtected()) {
            return httpMessageContext.responseUnauthorized();
        }

        return httpMessageContext.doNothing();
    }

    private AuthenticationStatus validateToken(String token, HttpMessageContext context) {
        try {
            if (!tokenProvider.validateToken(token)) {
                return context.responseUnauthorized();
            }
            var credential = tokenProvider.getCredential(token);
            return context.notifyContainerAboutLogin(credential.getPrincipal(), credential.getAuthorities());
        } catch (ExpiredJwtException ex) {
            LOG.info("Security exception for user {} - {}", ex.getClaims().getSubject(), ex.getMessage());
            return context.responseUnauthorized();
        }
    }

    private String extractToken(HttpMessageContext context) {
        var authorizationHeader = context.getRequest().getHeader("Authorization");
        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer")) {
            return authorizationHeader.substring("Bearer".length());
        }

        return null;
    }

}
