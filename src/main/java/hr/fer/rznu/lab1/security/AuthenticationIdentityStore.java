package hr.fer.rznu.lab1.security;

import hr.fer.rznu.lab1.dao.UserRepository;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.security.enterprise.credential.Credential;
import javax.security.enterprise.credential.UsernamePasswordCredential;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import static javax.security.enterprise.identitystore.CredentialValidationResult.INVALID_RESULT;
import static javax.security.enterprise.identitystore.CredentialValidationResult.NOT_VALIDATED_RESULT;
import javax.security.enterprise.identitystore.IdentityStore;
import static javax.security.enterprise.identitystore.IdentityStore.ValidationType.VALIDATE;
import org.mindrot.jbcrypt.BCrypt;

@RequestScoped
public class AuthenticationIdentityStore implements IdentityStore {

    public static final String PASSWORD_SALT = "$2a$10$WhGe9.jTom2Wer1LtAR6oO";

    @Inject
    private UserRepository userRepository;

    @Override
    public Set<ValidationType> validationTypes() {
        return Set.of(VALIDATE);
    }

    @Override
    public CredentialValidationResult validate(Credential credential) {
        CredentialValidationResult result = NOT_VALIDATED_RESULT;

        if (credential instanceof UsernamePasswordCredential) {
            UsernamePasswordCredential usernamePasswordCredential = (UsernamePasswordCredential) credential;
            String caller = usernamePasswordCredential.getCaller();
            String expectedPassword = userRepository
                    .getByUsername(caller)
                    .getPassword();
            String actualPasswordPlain = usernamePasswordCredential.getPasswordAsString();
            String actualPasswordHash = BCrypt.hashpw(actualPasswordPlain, PASSWORD_SALT);
            if (expectedPassword != null && expectedPassword.equals(actualPasswordHash)) {
                result = new CredentialValidationResult(caller);
            } else {
                result = INVALID_RESULT;
            }
        }

        return result;
    }

}
