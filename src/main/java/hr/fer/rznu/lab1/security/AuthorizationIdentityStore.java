package hr.fer.rznu.lab1.security;

import hr.fer.rznu.lab1.dao.UserRepository;
import static java.util.Collections.singleton;
import java.util.Set;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.security.enterprise.identitystore.CredentialValidationResult;
import javax.security.enterprise.identitystore.IdentityStore;
import static javax.security.enterprise.identitystore.IdentityStore.ValidationType.PROVIDE_GROUPS;

@RequestScoped
public class AuthorizationIdentityStore implements IdentityStore {

    @Inject
    private UserRepository userRepository;

    @Override
    public Set<String> getCallerGroups(CredentialValidationResult validationResult) {
        return userRepository.getByUsername(validationResult.getCallerPrincipal().getName()).getRoles();
    }

    @Override
    public Set<ValidationType> validationTypes() {
        return singleton(PROVIDE_GROUPS);
    }

}
