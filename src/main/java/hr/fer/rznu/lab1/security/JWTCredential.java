package hr.fer.rznu.lab1.security;

import java.util.Set;
import javax.security.enterprise.credential.Credential;

public class JWTCredential implements Credential {

    private final String      principal;
    private final Set<String> authorities;

    public JWTCredential(String subject, Set<String> authorities) {
        this.principal = subject;
        this.authorities = authorities;
    }

    public String getPrincipal() {
        return principal;
    }

    public Set<String> getAuthorities() {
        return authorities;
    }

}
