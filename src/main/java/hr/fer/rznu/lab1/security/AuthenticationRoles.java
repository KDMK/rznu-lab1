package hr.fer.rznu.lab1.security;

public class AuthenticationRoles {

    public static final String ADMIN = "ROLE_ADMIN";
    public static final String USER  = "ROLE_USER";

}
