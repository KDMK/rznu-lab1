package hr.fer.rznu.lab1.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import static java.util.concurrent.TimeUnit.HOURS;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import static org.apache.logging.log4j.LogManager.getLogger;
import org.apache.logging.log4j.Logger;

@Singleton
public class TokenProvider {

    private static final Logger LOG = getLogger(TokenProvider.class);

    private static final String AUTHORITIES_KEY = "auth";

    private String secretKey;
    private long   tokenValidity;

    @PostConstruct
    public void init() {
        this.secretKey = "my-secret-jwt-key";
        this.tokenValidity = HOURS.toSeconds(10);   //10 hours
    }

    public String createToken(String username, Set<String> authorities) {
        var now = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
        var validity = tokenValidity;
        var validUntil = Instant.ofEpochSecond(now + validity);

        return Jwts.builder()
                .setSubject(username)
                .claim(AUTHORITIES_KEY, String.join(",", authorities))
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .setExpiration(Date.from(validUntil))
                .compact();
    }

    public JWTCredential getCredential(String token) {
        var claims = Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(token)
                .getBody();

        var authorities = new HashSet<>(Arrays.asList(claims.get(AUTHORITIES_KEY).toString().split(",")));

        return new JWTCredential(claims.getSubject(), authorities);
    }

    public boolean validateToken(String authToken) {
        try {
            return Jwts.parser().setSigningKey(secretKey).parseClaimsJws(authToken) != null;
        } catch (SignatureException e) {
            LOG.info("Invalid JWT signature: {}", e.getMessage());
        }

        return false;
    }

}
