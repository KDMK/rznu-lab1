package hr.fer.rznu.lab1.model;

/**
 * Topic represents category of blog post. Every blog post must have category. User is responsible for determining
 * correct topic for post and create appropriate if such doesn't exist.
 */
public class Topic {

    private Long   id;
    private String name;

    /**
     * Instantiates a new Topic.
     */
    public Topic() {
    }

    /**
     * Copy constructor. Instantiates new Topic from existing one.
     *
     * @param other the other {@link Topic}
     */
    public Topic(Topic other) {
        this.id = other.id;
        this.name = other.name;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     * @return the id
     */
    public Topic setId(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     * @return the name
     */
    public Topic setName(String name) {
        this.name = name;
        return this;
    }

}
