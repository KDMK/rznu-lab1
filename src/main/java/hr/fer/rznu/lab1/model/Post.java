package hr.fer.rznu.lab1.model;

import java.time.LocalDateTime;

/**
 * Models blog post. Every post must have creator. Post can have multiple comments.
 */
public class Post {

    private Long          id;
    private String        title;
    private String        content;
    private Long          createdBy;
    private Long          topic;
    private LocalDateTime createdAt;

    /**
     * Instantiates a new Post.
     */
    public Post() {
    }

    /**
     * Copy constructor. Instantiates new Post from existing one.
     *
     * @param other the other {@link Post}
     */
    public Post(Post other) {
        this.id = other.id;
        this.title = other.title;
        this.content = other.title;
        this.createdBy = other.createdBy;
        this.topic = other.topic;
        this.createdAt = other.createdAt;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     * @return the id
     */
    public Post setId(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets title.
     *
     * @param title the title
     * @return the title
     */
    public Post setTitle(String title) {
        this.title = title;
        return this;
    }

    /**
     * Gets content.
     *
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * Sets content.
     *
     * @param content the content
     * @return the content
     */
    public Post setContent(String content) {
        this.content = content;
        return this;
    }

    /**
     * Gets created by.
     *
     * @return the created by
     */
    public Long getCreatedBy() {
        return createdBy;
    }

    /**
     * Sets created by.
     *
     * @param createdBy the created by
     * @return the created by
     */
    public Post setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    /**
     * Gets topic.
     *
     * @return the topic
     */
    public Long getTopic() {
        return topic;
    }

    /**
     * Sets topic.
     *
     * @param topic the topic
     * @return the topic
     */
    public Post setTopic(Long topic) {
        this.topic = topic;
        return this;
    }

    /**
     * Gets created at.
     *
     * @return the created at
     */
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    /**
     * Sets created at.
     *
     * @param createdAt the created at
     * @return the created at
     */
    public Post setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
        return this;
    }

}

