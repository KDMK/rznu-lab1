package hr.fer.rznu.lab1.model;

import static hr.fer.rznu.lab1.security.AuthenticationRoles.USER;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Models blog user. Username and password are interchangeable and must be unique.
 */
public class User {

    private Long        id;
    private String      username;
    private String      email;
    private String      password;
    private Set<String> roles;

    /**
     * Instantiates a new User.
     */
    public User() {
        this.roles = new HashSet<>();
        roles.add(USER);
    }

    /**
     * Copy constructor. Instantiates new User from existing one.
     *
     * @param user the user
     */
    public User(User user) {
        this();
        this.id = user.getId();
        this.username = user.getUsername();
        this.email = user.getEmail();
        this.password = user.getPassword();
        this.roles = user.getRoles();
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     * @return the id
     */
    public User setId(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Gets username.
     *
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets username.
     *
     * @param username the username
     * @return the username
     */
    public User setUsername(String username) {
        this.username = username;
        return this;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     * @return the email
     */
    public User setEmail(String email) {
        this.email = email;
        return this;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     * @return the password
     */
    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public User setRoles(Set<String> roles) {
        this.roles = roles;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(username, user.username) &&
                Objects.equals(email, user.email) &&
                Objects.equals(password, user.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, email, password);
    }

}
