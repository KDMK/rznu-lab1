package hr.fer.rznu.lab1.dao;

import hr.fer.rznu.lab1.model.User;
import java.util.List;

/**
 * Models interface for communicating with repository that holds user data.
 */
public interface UserRepository {

    /**
     * Finds all users in repository.
     *
     * @return List of users or empty list if none are present.
     */
    List<User> getAll();

    /**
     * Finds user with given id
     *
     * @param id user's id
     * @return User object if found
     * @throws javax.persistence.EntityNotFoundException if user with requested id don't exist
     */
    User getOne(Long id);

    /**
     * Finds user with given username
     *
     * @param username user's username
     * @return User object if found
     * @throws javax.persistence.EntityNotFoundException if user with requested username don't exist
     */
    User getByUsername(String username);

    /**
     * Finds user with given email
     *
     * @param email user's email
     * @return User object if found
     * @throws javax.persistence.EntityNotFoundException if user with requested email don't exist
     */
    User getByEmail(String email);

    /**
     * Inserts new user in database
     *
     * @param user object to be inserted
     * @return User object with updated id;
     * @throws hr.fer.rznu.lab1.exception.DataIntegrityViolationException if there is collision in user id
     */
    User insert(User user);

    /**
     * Updates user in database
     *
     * @param user object to be inserted
     * @return Updated User object
     * @throws IllegalArgumentException if users isn't present in database
     */
    User update(User user);

    /**
     * Deletes user from database
     *
     * @param id id of a user
     * @throws javax.persistence.EntityNotFoundException if user with given id doesn't exist
     */
    void delete(Long id);

}
