package hr.fer.rznu.lab1.dao.impl;

import hr.fer.rznu.lab1.dao.TopicRepository;
import hr.fer.rznu.lab1.exception.DataIntegrityViolationException;
import hr.fer.rznu.lab1.model.Topic;
import static java.lang.String.format;
import java.util.ArrayList;
import static java.util.Collections.emptyList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import javax.ejb.Singleton;
import javax.persistence.EntityNotFoundException;
import org.apache.commons.text.WordUtils;
import static org.apache.logging.log4j.LogManager.getLogger;
import org.apache.logging.log4j.Logger;

/**
 * {@inheritDoc}
 */
@Singleton
public class InMemoryTopicRepository implements TopicRepository {

    private static final Logger LOG = getLogger(InMemoryTopicRepository.class);

    private static final String ERROR_UNKNOWN_ID     = "Topic with id %d doesn't exist";
    private static final String ERROR_DEFINED_ID     = "Cannot create topic with defined id";
    private static final String ERROR_DUPLICATE_NAME = "Topic with given name already exists";

    private Map<Long, Topic> db            = new HashMap<>();
    private AtomicLong       topicSequence = new AtomicLong(1);

    @Override
    public List<Topic> getAll() {
        LOG.info("Fetching all topics.");
        if (db.size() > 0) {
            LOG.info("No users present in database");
            return new ArrayList<>(db.values());
        }

        return emptyList();
    }

    @Override
    public Topic getOne(Long id) {
        LOG.info("Fetching topic with id {}", id);
        Topic topic = db.get(id);
        if (topic == null) {
            LOG.error("Cannot find topic with id {}", id);
            throw new EntityNotFoundException(format(ERROR_UNKNOWN_ID, id));
        }

        return new Topic(topic);
    }

    @Override
    public Topic create(Topic topic) {
        if (topic.getId() != null) {
            throw new DataIntegrityViolationException(ERROR_DEFINED_ID);
        }

        var topicName = WordUtils.capitalize(topic.getName());
        if (db.values().stream().anyMatch(topic1 -> topicName.equals(topic1.getName()))) {
            throw new DataIntegrityViolationException(ERROR_DUPLICATE_NAME);
        }

        var newId = topicSequence.getAndIncrement();
        topic.setId(newId);
        var persistedTopic = new Topic(topic);
        db.put(newId, persistedTopic);
        return persistedTopic;
    }

    @Override
    public void delete(Long id) {
        if (!db.containsKey(id)) {
            throw new EntityNotFoundException(format(ERROR_UNKNOWN_ID, id));
        }

        db.remove(id);
    }

    @Override
    public Topic update(Topic existingTopic) {
        var topicName = WordUtils.capitalize(existingTopic.getName());
        if (db.values().stream().anyMatch(topic1 -> topicName.equals(topic1.getName()))) {
            throw new DataIntegrityViolationException(ERROR_DUPLICATE_NAME);
        }

        db.put(existingTopic.getId(), existingTopic);
        return existingTopic;
    }

}
