package hr.fer.rznu.lab1.dao.impl;

import hr.fer.rznu.lab1.dao.PostRepository;
import hr.fer.rznu.lab1.dao.TopicRepository;
import hr.fer.rznu.lab1.dao.UserRepository;
import hr.fer.rznu.lab1.exception.ConstraintViolationException;
import hr.fer.rznu.lab1.exception.DataIntegrityViolationException;
import hr.fer.rznu.lab1.model.Post;
import static java.lang.String.format;
import java.util.ArrayList;
import static java.util.Collections.emptyList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import static java.util.Objects.requireNonNull;
import java.util.concurrent.atomic.AtomicLong;
import static java.util.stream.Collectors.toList;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;
import static org.apache.logging.log4j.LogManager.getLogger;
import org.apache.logging.log4j.Logger;

/**
 * {@inheritDoc}
 */
@Singleton
public class InMemoryPostRepository implements PostRepository {

    private static final Logger LOG                = getLogger(InMemoryPostRepository.class);
    private static final String ERROR_UNKNOWN_ID   = "Post with id %d doesn't exist";
    private static final String ERROR_DEFINED_ID   = "Cannot create post with defined id";
    private static final int    DEF_MIN_CONSTRAINT = 0;
    private static final int    DEF_MAX_CONSTRAINT = 255;

    @Inject
    private UserRepository  userRepository;
    @Inject
    private TopicRepository topicRepository;

    private Map<Long, Post> db           = new HashMap<>();
    private AtomicLong      postSequence = new AtomicLong(1);

    private static void nullCheck(Object field, String fieldName) {
        if (field != null) {
            return;
        }
        throw new ConstraintViolationException(format("Post %s cannot null", fieldName));
    }

    private static void isValidSize(String fieldValue, String fieldName, Integer min, Integer max) {
        min = min != null ? min : DEF_MIN_CONSTRAINT;
        max = max != null ? min : DEF_MAX_CONSTRAINT;

        if (fieldValue.length() < min && fieldValue.length() > max) {
            throw new ConstraintViolationException(format("Post %s must be between %d and %d", fieldName, min, max));
        }
    }

    private static void isValidSize(String field, String fieldName) {
        isValidSize(field, fieldName, null, null);
    }

    @Override
    public List<Post> getAll() {
        LOG.info("Fetching all posts.");
        if (db.size() > 0) {
            LOG.info("No posts present in database");
            return new ArrayList<>(db.values());
        }

        return emptyList();
    }

    @Override
    public Post getOne(Long id) {
        LOG.info("Fetching post with id {}", id);
        Post post = db.get(id);
        if (post == null) {
            LOG.error("Cannot find post with id {}", id);
            throw new EntityNotFoundException(format(ERROR_UNKNOWN_ID, id));
        }

        return new Post(post);
    }

    @Override
    public List<Post> getByUser(Long userId) {
        requireNonNull(userId);
        checkUserExists(userId);
        return db.values()
                .stream()
                .filter(post -> Objects.equals(post.getCreatedBy(), userId))
                .collect(toList());
    }

    @Override
    public List<Post> getByTopic(Long topicId) {
        requireNonNull(topicId);
        checkTopicExists(topicId);
        return db.values()
                .stream()
                .filter(post -> Objects.equals(post.getTopic(), topicId))
                .collect(toList());
    }

    @Override
    public Post create(Post post) {
        if (post.getId() != null) {
            throw new DataIntegrityViolationException(ERROR_DEFINED_ID);
        }

        nullCheck(post.getTopic(), "topic");
        nullCheck(post.getTitle(), "title");
        nullCheck(post.getContent(), "content");
        nullCheck(post.getCreatedBy(), "createdBy");

        isValidSize(post.getTitle(), "title");
        isValidSize(post.getContent(), "content");

        checkUserExists(post.getCreatedBy());
        checkTopicExists(post.getTopic());

        var id = postSequence.getAndIncrement();
        post.setId(id);
        var persistedValue = new Post(post);
        db.put(id, persistedValue);
        return persistedValue;
    }

    @Override
    public void delete(Long id) {
        if (!db.containsKey(id)) {
            throw new EntityNotFoundException(format(ERROR_UNKNOWN_ID, id));
        }

        db.remove(id);
    }

    @Override
    public Post updatePost(Post existingPost) {
        checkTopicExists(existingPost.getTopic());
        checkUserExists(existingPost.getCreatedBy());
        isValidSize(existingPost.getContent(), "content");
        isValidSize(existingPost.getTitle(), "title");

        var newPost = new Post(existingPost);
        db.put(existingPost.getId(), newPost);
        return newPost;
    }

    private void checkTopicExists(Long topicId) {
        try {
            topicRepository.getOne(topicId);
        } catch (EntityNotFoundException ex) {
            LOG.error(ex.getMessage());
            throw ex;
        }
    }

    private void checkUserExists(Long userId) {
        try {
            userRepository.getOne(userId);
        } catch (EntityNotFoundException ex) {
            LOG.error(ex.getMessage());
            throw ex;
        }
    }

}
