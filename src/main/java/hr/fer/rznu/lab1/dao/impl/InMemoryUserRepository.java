package hr.fer.rznu.lab1.dao.impl;

import hr.fer.rznu.lab1.dao.UserRepository;
import hr.fer.rznu.lab1.exception.DataIntegrityViolationException;
import hr.fer.rznu.lab1.model.User;
import static hr.fer.rznu.lab1.security.AuthenticationIdentityStore.PASSWORD_SALT;
import static hr.fer.rznu.lab1.security.AuthenticationRoles.ADMIN;
import static hr.fer.rznu.lab1.security.AuthenticationRoles.USER;
import static java.lang.String.format;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.persistence.EntityNotFoundException;
import static org.apache.commons.text.WordUtils.capitalize;
import static org.apache.logging.log4j.LogManager.getLogger;
import org.apache.logging.log4j.Logger;
import org.mindrot.jbcrypt.BCrypt;

/**
 * {@inheritDoc}
 */
@Singleton
public class InMemoryUserRepository implements UserRepository {

    private static final Logger LOG = getLogger(InMemoryUserRepository.class);

    private static final String ERROR_UNKNOWN_ID       = "User with id %d doesn't exist";
    private static final String ERROR_UNKNOWN_USERNAME = "User with username %s doesn't exist";
    private static final String ERROR_UNKNOWN_EMAIL    = "User with email %s doesn't exist";

    private static final String EMAIL_INDEX    = "email";
    private static final String USERNAME_INDEX = "username";

    private Map<Long, User> db           = new HashMap<>();
    private AtomicLong      userSequence = new AtomicLong(1);

    private Map<String, Map<Object, User>> indexes = new HashMap<>();

    @PostConstruct
    public void init() {
        var admin = new User()
                .setEmail("admin@test.net")
                .setUsername("admin")
                .setRoles(Set.of(ADMIN, USER))
                .setPassword(BCrypt.hashpw("admin", PASSWORD_SALT));
        this.insert(admin);
    }

    /**
     * Creates index on passed field(indexName) for fast retrieval of objects.
     *
     * @param indexName field on which we are building index
     * @param clazz class of object on which we are building index
     * @throws IllegalAccessException if method we try to invoke has wrong visibility
     * @throws NoSuchMethodException if getter method we try to invoke doesn't exist
     * @throws InvocationTargetException if object on which we try to invoke method don't have requested method
     */
    private static <K, T> void createIndex(String indexName, Map<String, Map<Object, T>> idxMap,
            Map<K, T> originalDb, Class<T> clazz)
            throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        if (idxMap.containsKey(indexName)) {
            return;
        }

        var index = new HashMap<Object, T>();
        var indexFieldGetter = clazz.getDeclaredMethod(format("get%s", capitalize(indexName)));
        for (T k : originalDb.values()) {
            var fieldValue = indexFieldGetter.invoke(k);
            index.put(fieldValue, k);
        }

        idxMap.put(indexName, index);
    }

    private static void updateIndex(User user, Map<String, Map<Object, User>> indexes) {
        for (Map.Entry<String, Map<Object, User>> es : indexes.entrySet()) {
            var methodName = format("get%s", capitalize(es.getKey()));
            Method getter;
            Object keyToUpdate;
            try {
                getter = User.class.getMethod(methodName);
                keyToUpdate = getter.invoke(user);
            } catch (Exception e) {
                LOG.error("Cannot update user {} in user_index[{}]", user, es.getKey(), e);
                continue;
            }

            es.getValue().put(keyToUpdate, user);
        }
    }

    private static void removeFromIndex(User user, Map<String, Map<Object, User>> indexes) {
        for (Map.Entry<String, Map<Object, User>> es : indexes.entrySet()) {
            var methodName = format("get%s", capitalize(es.getKey()));
            Method getter;
            Object keyToDelete;
            try {
                getter = User.class.getMethod(methodName);
                keyToDelete = getter.invoke(user);
            } catch (Exception e) {
                LOG.error("Cannot delete user {} from user_index[{}]", user, es.getKey(), e);
                continue;
            }

            es.getValue().remove(keyToDelete);
        }
    }

    @Override
    public List<User> getAll() {
        LOG.info("Fetching all users");
        if (db.size() > 0) {
            LOG.info("No users present in database");
            return new ArrayList<>(db.values());
        }

        return Collections.emptyList();
    }

    @Override
    public User getOne(Long id) {
        LOG.info("Fetching user with id {}", id);
        User user = db.get(id);
        if (user == null) {
            LOG.error("Cannot find user with id {}", id);
            throw new EntityNotFoundException(format(ERROR_UNKNOWN_ID, id));
        }

        return new User(user);
    }

    @Override
    public User getByUsername(String username) {
        return getUserByIndex(username, USERNAME_INDEX, ERROR_UNKNOWN_USERNAME);
    }

    @Override
    public User getByEmail(String email) {
        return getUserByIndex(email, EMAIL_INDEX, ERROR_UNKNOWN_EMAIL);
    }

    private User getUserByIndex(String field, String indexName, String errorTemplate) {
        if (!indexes.containsKey(indexName)) {
            try {
                LOG.info("Creating {} index on {} table", field, User.class.getName());
                createIndex(indexName, indexes, db, User.class);
            } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
                LOG.error(format(errorTemplate, field), e);
            }
        }
        User user = indexes.get(indexName).get(field);

        if (user == null) {
            LOG.error(format(errorTemplate, field));
            throw new EntityNotFoundException(format(errorTemplate, field));
        }

        return new User(user);
    }

    @Override
    public User insert(User user) {
        LOG.info("Inserting user {}", user);
        if (user.getId() != null) {
            LOG.error("Cannot create user with defined id");
            throw new DataIntegrityViolationException("Cannot insert user with defined id.");
        }

        try {
            getByUsername(user.getUsername());
            throw new DataIntegrityViolationException("User with username " + user.getUsername() + " already exist");
        } catch (EntityNotFoundException ignored) {
        }

        try {
            getByEmail(user.getEmail());
            throw new DataIntegrityViolationException("User with email " + user.getEmail() + " already exist");
        } catch (EntityNotFoundException ignored) {
        }

        var id = this.userSequence.getAndIncrement();
        User insertedUser = new User(user);
        insertedUser.setId(id);
        db.put(id, insertedUser);
        updateIndex(insertedUser, indexes);

        return insertedUser;
    }

    @Override
    public User update(User updatedUser) {
        User dbUser;
        try {
            dbUser = this.getOne(updatedUser.getId());
        } catch (Exception e) {
            LOG.error("User with id {} not present in database", updatedUser.getId());
            throw new IllegalArgumentException("User not present in database!");
        }

        User newUser = new User(updatedUser);
        db.put(dbUser.getId(), newUser);
        updateIndex(newUser, indexes);
        return newUser;
    }

    @Override
    public void delete(Long id) {
        if (!db.containsKey(id)) {
            throw new EntityNotFoundException(format("User with id %d not found in database", id));
        }

        var user = db.get(id);
        removeFromIndex(user, indexes);
        db.remove(user.getId());
    }

}
