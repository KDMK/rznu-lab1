package hr.fer.rznu.lab1.dao;

import hr.fer.rznu.lab1.model.Topic;
import java.util.List;

/**
 * The interface Topic repository model interface for communication with data store that holds {@link Topic} data
 */
public interface TopicRepository {

    /**
     * Gets all {@link Topic}.
     *
     * @return List of {@link Topic} or empty list if there is none present
     */
    List<Topic> getAll();

    /**
     * Finds {@link Topic} by given id.
     *
     * @param id the id of {@link Topic}
     * @return {@link Topic} object if present
     * @throws javax.persistence.EntityNotFoundException if {@link Topic} with requested id don't exist
     */
    Topic getOne(Long id);

    /**
     * Inserts new {@link Topic} in data store
     *
     * @param topic the {@link Topic} object to be inserted
     * @return {@link Topic} with assigned id
     */
    Topic create(Topic topic);

    /**
     * Deletes topic with given id
     *
     * @param id the id of the {@link Topic}
     * @throws javax.persistence.EntityNotFoundException if {@link Topic} with given id doesn't exist
     */
    void delete(Long id);

    /**
     * Updates existing topic
     *
     * @param existingTopic existing topic
     * @return Updated topic
     */
    Topic update(Topic existingTopic);

}
