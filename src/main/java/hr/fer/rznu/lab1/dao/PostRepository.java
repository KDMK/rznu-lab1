package hr.fer.rznu.lab1.dao;

import hr.fer.rznu.lab1.model.Post;
import java.util.List;

/**
 * The interface Post repository.
 */
public interface PostRepository {

    /**
     * Gets all {@link Post}.
     *
     * @return List of {@link Post} or emty list if there is none present
     */
    List<Post> getAll();

    /**
     * Finds {@link Post} by given id
     *
     * @param id the id of {@link Post}
     * @return {@link Post} object if present
     */
    Post getOne(Long id);

    /**
     * Finds all {@link Post} by given user id.
     *
     * @param userId the user id
     * @return List of Post for requested user
     * @throws javax.persistence.EntityNotFoundException if there is no such user in database
     */
    List<Post> getByUser(Long userId);

    /**
     * Finds all {@link Post} by given topic id.
     *
     * @param topicId the topic id
     * @return List of Post for requested topic
     * @throws javax.persistence.EntityNotFoundException if there is no such topic in database
     */
    List<Post> getByTopic(Long topicId);

    /**
     * Inserts new {@link Post} in data store
     *
     * @param post the {@link Post} object to be inserted
     * @return {@link Post} with assigned id
     */
    Post create(Post post);

    /**
     * Deletes topic with given id
     *
     * @param id the id of the {@link Post}
     * @throws javax.persistence.EntityNotFoundException if {@link Post} with given id doesn't exist
     */
    void delete(Long id);

    /**
     * Updates existing topic
     *
     * @param existingPost post object
     * @return updated post if update was successful
     */
    Post updatePost(Post existingPost);

}
