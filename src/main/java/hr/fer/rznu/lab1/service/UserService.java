package hr.fer.rznu.lab1.service;

import hr.fer.rznu.lab1.model.User;
import hr.fer.rznu.lab1.web.dto.CreateUserDto;
import hr.fer.rznu.lab1.web.dto.UpdateUserDto;
import java.util.List;

/**
 * The interface User service.
 */
public interface UserService {

    /**
     * Gets all users.
     *
     * @return the all users
     */
    List<User> getAllUsers();

    /**
     * Gets one.
     *
     * @param id the id
     * @return the one
     */
    User getOne(Long id);

    /**
     * Gets one.
     *
     * @param username the username
     * @return the one
     */
    User getOne(String username);

    /**
     * Gets one by email.
     *
     * @param email the email
     * @return the one by email
     */
    User getOneByEmail(String email);

    /**
     * Delete.
     *
     * @param id the id
     */
    void delete(Long id);

    /**
     * Update user user.
     *
     * @param user the user
     * @return the user
     */
    User updateUser(UpdateUserDto user);

    /**
     * Create user user.
     *
     * @param user the user
     * @return the user
     */
    User createUser(CreateUserDto user);

    /**
     * Add role to user.
     *
     * @param userId the user id
     * @param role the role
     * @return the user
     */
    User addRoleToUser(Long userId, String role);

    /**
     * Remove role from user user.
     *
     * @param id the id
     * @param role the role
     * @return the user
     */
    User removeRoleFromUser(Long id, String role);

}
