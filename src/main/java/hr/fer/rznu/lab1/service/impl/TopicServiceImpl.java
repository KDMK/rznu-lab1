package hr.fer.rznu.lab1.service.impl;

import hr.fer.rznu.lab1.dao.TopicRepository;
import hr.fer.rznu.lab1.model.Topic;
import hr.fer.rznu.lab1.service.TopicService;
import hr.fer.rznu.lab1.web.dto.CreateTopicDto;
import hr.fer.rznu.lab1.web.dto.UpdateTopicDto;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class TopicServiceImpl implements TopicService {

    @Inject
    private TopicRepository topicRepository;

    @Override
    public List<Topic> getAllTopics() {
        return topicRepository.getAll();
    }

    @Override
    public Topic getTopic(Long id) {
        return topicRepository.getOne(id);
    }

    @Override
    public Topic createTopic(CreateTopicDto createTopicDto) {
        var topic = new Topic();
        topic.setName(createTopicDto.getName());

        return topicRepository.create(topic);
    }

    @Override
    public Topic updateTopic(Long topicId, UpdateTopicDto updateTopicDto) {
        var existingTopic = topicRepository.getOne(topicId);
        if(updateTopicDto.getName() != null && updateTopicDto.getName().trim().length() > 0) {
            existingTopic.setName(updateTopicDto.getName());
        }

        return topicRepository.update(existingTopic);
    }

    @Override
    public void deleteTopic(Long id) {

    }

}
