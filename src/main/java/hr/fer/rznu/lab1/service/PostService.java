package hr.fer.rznu.lab1.service;

import hr.fer.rznu.lab1.web.dto.CreatePostDto;
import hr.fer.rznu.lab1.web.dto.PostDto;
import hr.fer.rznu.lab1.web.dto.UpdatePostDto;
import java.util.List;

public interface PostService {

    List<PostDto> getAll();

    PostDto getPost(Long id);

    List<PostDto> getPostsByTopic(Long topicId);

    List<PostDto> getPostsByUser(Long userId);

    PostDto createNewPost(CreatePostDto createPostDto);

    PostDto updatePost(Long postId, UpdatePostDto updatePostDto);

    void deletePost(Long postId);

}
