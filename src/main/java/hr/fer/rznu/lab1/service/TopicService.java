package hr.fer.rznu.lab1.service;

import hr.fer.rznu.lab1.model.Topic;
import hr.fer.rznu.lab1.web.dto.CreateTopicDto;
import hr.fer.rznu.lab1.web.dto.UpdateTopicDto;
import java.util.List;

public interface TopicService {

    List<Topic> getAllTopics();

    Topic getTopic(Long id);

    Topic createTopic(CreateTopicDto createTopicDto);

    Topic updateTopic(Long topicId, UpdateTopicDto updateTopicDto);

    void deleteTopic(Long id);

}
