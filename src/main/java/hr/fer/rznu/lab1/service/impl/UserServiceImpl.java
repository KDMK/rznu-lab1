package hr.fer.rznu.lab1.service.impl;

import hr.fer.rznu.lab1.dao.UserRepository;
import hr.fer.rznu.lab1.model.User;
import static hr.fer.rznu.lab1.security.AuthenticationIdentityStore.PASSWORD_SALT;
import hr.fer.rznu.lab1.service.UserService;
import hr.fer.rznu.lab1.web.dto.CreateUserDto;
import hr.fer.rznu.lab1.web.dto.UpdateUserDto;
import java.util.HashSet;
import java.util.List;
import javax.inject.Inject;
import javax.inject.Singleton;
import org.mindrot.jbcrypt.BCrypt;

@Singleton
public class UserServiceImpl implements UserService {

    @Inject
    private UserRepository userRepository;

    @Override
    public List<User> getAllUsers() {
        return userRepository.getAll();
    }

    @Override
    public User getOne(Long id) {
        return userRepository.getOne(id);
    }

    @Override
    public User getOne(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public User getOneByEmail(String email) {
        return userRepository.getByEmail(email);
    }

    @Override
    public void delete(Long id) {
        this.userRepository.delete(id);
    }

    @Override
    public User updateUser(UpdateUserDto updateUserDto) {
        var user = new User()
                .setId(updateUserDto.getId())
                .setUsername(updateUserDto.getUsername())
                .setEmail(updateUserDto.getEmail())
                .setPassword(BCrypt.hashpw(updateUserDto.getPassword(), PASSWORD_SALT))
                .setRoles(updateUserDto.getRoles());

        return userRepository.update(user);
    }

    @Override
    public User createUser(CreateUserDto createUserDto) {
        var user = new User()
                .setUsername(createUserDto.getUsername())
                .setEmail(createUserDto.getEmail())
                .setPassword(BCrypt.hashpw(createUserDto.getPassword(), PASSWORD_SALT));

        if(createUserDto.getRoles() != null && createUserDto.getRoles().size() > 0) {
            user.setRoles(createUserDto.getRoles());
        }

        return userRepository.insert(user);
    }

    @Override
    public User addRoleToUser(Long userId, String role) {
        var user = getOne(userId);

        var roles = new HashSet<>(user.getRoles());
        roles.add(role);

        var updateUserDto = new UpdateUserDto(user, roles);
        return updateUser(updateUserDto);
    }

    @Override
    public User removeRoleFromUser(Long id, String role) {
        var user = getOne(id);

        var roles = new HashSet<>(user.getRoles());
        roles.remove(role);

        var updateUserDto = new UpdateUserDto(user, roles);
        return updateUser(updateUserDto);
    }

}
