package hr.fer.rznu.lab1.service.impl;

import hr.fer.rznu.lab1.dao.PostRepository;
import hr.fer.rznu.lab1.model.Post;
import hr.fer.rznu.lab1.service.PostService;
import hr.fer.rznu.lab1.service.TopicService;
import hr.fer.rznu.lab1.service.UserService;
import hr.fer.rznu.lab1.web.dto.CreatePostDto;
import hr.fer.rznu.lab1.web.dto.PostDto;
import hr.fer.rznu.lab1.web.dto.UpdatePostDto;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;
import javax.ejb.Singleton;
import javax.inject.Inject;

@Singleton
public class PostServiceImpl implements PostService {

    @Inject
    private PostRepository postRepository;

    @Inject
    private UserService userService;

    @Inject
    private TopicService topicService;

    @Override
    public List<PostDto> getAll() {
        return postRepository.getAll()
                .stream()
                .map(this::transformToDto)
                .collect(Collectors.toList());
    }

    @Override
    public PostDto getPost(Long id) {
        return transformToDto(postRepository.getOne(id));
    }

    @Override
    public List<PostDto> getPostsByTopic(Long topicId) {
        return postRepository.getByTopic(topicId)
                .stream()
                .map(this::transformToDto)
                .collect(Collectors.toList());
    }

    @Override
    public List<PostDto> getPostsByUser(Long userId) {
        return postRepository.getByUser(userId)
                .stream()
                .map(this::transformToDto)
                .collect(Collectors.toList());
    }

    @Override
    public PostDto createNewPost(CreatePostDto createPostDto) {
        var post = new Post();
        post.setTitle(createPostDto.getTitle());
        post.setContent(createPostDto.getContent());
        post.setCreatedAt(LocalDateTime.now());
        post.setCreatedBy(createPostDto.getCreatedBy());
        post.setTopic(createPostDto.getTopicId());

        return transformToDto(postRepository.create(post));
    }

    @Override
    public PostDto updatePost(Long postId, UpdatePostDto updatePostDto) {
        var existingPost = postRepository.getOne(postId);

        if (updatePostDto.getContent() != null && updatePostDto.getContent().trim().length() != 0) {
            existingPost.setContent(updatePostDto.getContent());
        }
        if (updatePostDto.getTitle() != null && updatePostDto.getTitle().trim().length() != 0) {
            existingPost.setTitle(updatePostDto.getTitle());
        }
        if (updatePostDto.getTopic() != null) {
            existingPost.setTopic(updatePostDto.getTopic());
        }

        return transformToDto(postRepository.updatePost(existingPost));
    }

    @Override
    public void deletePost(Long postId) {
        postRepository.delete(postId);
    }

    private PostDto transformToDto(Post post) {
        var postDto = new PostDto();
        var postUser = userService.getOne(post.getCreatedBy());
        var postTopic = topicService.getTopic(post.getTopic());

        postDto.setId(post.getId());
        postDto.setTitle(post.getTitle());
        postDto.setContent(post.getContent());
        postDto.setCreatedBy(postUser);
        postDto.setTopic(postTopic);
        postDto.setCreatedAt(post.getCreatedAt());

        return postDto;
    }

}
