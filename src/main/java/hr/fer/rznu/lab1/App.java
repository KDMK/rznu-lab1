package hr.fer.rznu.lab1;

import static hr.fer.rznu.lab1.security.AuthenticationRoles.ADMIN;
import static hr.fer.rznu.lab1.security.AuthenticationRoles.USER;
import io.swagger.annotations.Contact;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;
import javax.annotation.security.DeclareRoles;
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/api")
@DeclareRoles({ USER, ADMIN })
@SwaggerDefinition(info = @Info(
        title = "SOC Lab1 example REST API",
        description = "An example API for SOC lab assignment 1",
        version = "1.0.0",
        contact = @Contact(
                name = "Matija Bartolac",
                email = "matija.bartolac@gmail.com",
                url = "https://gitlab.com/KDMK"
        )
))
public class App extends Application {
}
