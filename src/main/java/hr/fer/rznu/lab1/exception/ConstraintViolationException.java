package hr.fer.rznu.lab1.exception;

/**
 * The type Constraint violation exception indicates that there was mismatch between value that we
 * want to store and expected value(eg. too long string, empty string etc.)
 */
public class ConstraintViolationException extends RuntimeException {

    /**
     * Instantiates a new Constraint violation exception.
     *
     * @param message the message
     */
    public ConstraintViolationException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Constraint violation exception.
     *
     * @param message the message
     * @param cause the cause
     */
    public ConstraintViolationException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new Constraint violation exception.
     *
     * @param cause the cause
     */
    public ConstraintViolationException(Throwable cause) {
        super(cause);
    }

}
