package hr.fer.rznu.lab1.exception;

/**
 * The type Data integrity violation exception indicates that there is some integrity constraint
 * is violated.
 */
public class DataIntegrityViolationException extends RuntimeException {

    /**
     * Instantiates a new Data integrity violation exception.
     *
     * @param message the message
     */
    public DataIntegrityViolationException(String message) {
        super(message);
    }

    /**
     * Instantiates a new Data integrity violation exception.
     *
     * @param message the message
     * @param cause the cause
     */
    public DataIntegrityViolationException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Instantiates a new Data integrity violation exception.
     *
     * @param cause the cause
     */
    public DataIntegrityViolationException(Throwable cause) {
        super(cause);
    }

}
