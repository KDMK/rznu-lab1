package hr.fer.rznu.lab1.dao;

import hr.fer.rznu.lab1.dao.impl.InMemoryUserRepository;
import hr.fer.rznu.lab1.exception.DataIntegrityViolationException;
import hr.fer.rznu.lab1.model.User;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityNotFoundException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class InMemoryUserRepositoryTest {

    private UserRepository userRepository;

    @Before
    public void initialize() throws NoSuchFieldException, IllegalAccessException {
        this.userRepository = new InMemoryUserRepository();

        var user1 = new User().setId(1L).setUsername("pperic").setEmail("pperic@fer.hr").setPassword("obscured");
        var user2 = new User().setId(2L).setUsername("iivic").setEmail("iivic@fer.hr").setPassword("obscured");
        var user3 = new User().setId(1L).setUsername("aanic").setEmail("aanic@fer.hr").setPassword("obscured");

        Field db = userRepository.getClass().getDeclaredField("db");
        Field userSequence = userRepository.getClass().getDeclaredField("userSequence");
        db.setAccessible(true);
        userSequence.setAccessible(true);

        HashMap<Long, User> realDb = (HashMap<Long, User>) db.get(userRepository);
        realDb.put(1L, user1);
        realDb.put(2L, user2);
        realDb.put(3L, user3);

        AtomicLong realUserSequence = (AtomicLong) userSequence.get(userRepository);
        realUserSequence.set(4L);

        db.setAccessible(false);
        userSequence.setAccessible(false);
    }

    @Test
    public void createUser_validUser_shouldCreate() {
        var user = new User();
        user.setUsername("mmarkovic").setEmail("mmarkovic@fer.hr").setPassword("obscured");

        var createdUser = userRepository.insert(user);
        assertNotNull(createdUser.getId());
        assertEquals("Created user's and passed user's username doesn't match", user.getUsername(),
                createdUser.getUsername());
        assertEquals("Created user's and passed user's email doesn't match", user.getEmail(), createdUser.getEmail());
        assertEquals("Created user's and passed user's password doesn't match", user.getPassword(),
                createdUser.getPassword());
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void createUser_userWithId_shouldThrow() {
        var user = new User();
        user.setId(1L).setUsername("mmarkovic").setEmail("mmarkovic@fer.hr").setPassword("obscured");

        userRepository.insert(user);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void createUser_duplicateUsername_shouldThrow() {
        var user = new User();
        user.setUsername("mmarkovic").setEmail("mmarkovic@fer.hr").setPassword("obscured");

        userRepository.insert(user);
        userRepository.insert(user);
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void createUser_duplicateEmail_shouldThrow() {
        var user = new User();
        user.setUsername("mmarkovic").setEmail("mmarkovic@fer.hr").setPassword("obscured");

        userRepository.insert(user);
        userRepository.insert(user);
    }

    @Test
    public void createUser_indexPresent_shouldUpdateIndex() {
        userRepository.getByUsername("pperic");
        userRepository.getByEmail("pperic@fer.hr");

        var user = new User();
        user.setUsername("mmarkovic").setEmail("mmarkovic@fer.hr").setPassword("obscured");

        var createdUser = userRepository.insert(user);
        assertNotNull(createdUser.getId());
        assertEquals("Created user's and passed user's username doesn't match", user.getUsername(),
                createdUser.getUsername());
        assertEquals("Created user's and passed user's email doesn't match", user.getEmail(), createdUser.getEmail());
        assertEquals("Created user's and passed user's password doesn't match", user.getPassword(),
                createdUser.getPassword());

        var userByUsername = userRepository.getByUsername(user.getUsername());
        var userByEmail = userRepository.getByEmail(user.getEmail());

        assertEquals(createdUser, userByEmail);
        assertEquals(createdUser, userByUsername);
    }

    @Test
    public void update_validUser_shouldUpdate() {
        var oldUser = userRepository.getOne(1L);
        var userToUpdate = new User(oldUser);
        userToUpdate.setUsername("test");

        var newUser = userRepository.update(userToUpdate);
        assertNotEquals(oldUser.getUsername(), newUser.getUsername());
    }

    @Test(expected = IllegalArgumentException.class)
    public void update_invalidUser_shouldThrow() {
        var user = new User()
                .setId(6L)
                .setEmail("testan@testovski.net")
                .setUsername("testan");

        userRepository.update(user);
    }

    @Test
    public void update_indexPresent_shouldUpdate() {
        var oldUser = userRepository.getOne(1L);
        var userToUpdate = new User(oldUser);
        userToUpdate.setUsername("test");

        var updatedUser = userRepository.update(userToUpdate);
        assertNotEquals(oldUser.getUsername(), updatedUser.getUsername());

        var updatedByUsername = userRepository.getByUsername("test");
        var updatedByEmail = userRepository.getByEmail(oldUser.getEmail());

        assertEquals(updatedUser, updatedByEmail);
        assertEquals(updatedUser, updatedByUsername);
    }

    @Test
    public void getAll_usersPresent_shouldReturnNonEmpty() {
        var users = userRepository.getAll();
        assertFalse("List of returned users is empty", users.isEmpty());
    }

    @Test
    public void getAll_usersNotPresent_shouldReturnEmpty() {
        var otherDb = new InMemoryUserRepository();
        var users = otherDb.getAll();
        assertTrue("Users list should be empty", users.isEmpty());
    }

    @Test
    public void getOne_validId_shouldReturnUser() {
        var user = userRepository.getOne(1L);
        assertNotNull(user);
        assertEquals((Long) 1L, user.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getOne_invalidId_shouldThrow() {
        userRepository.getOne(1000L);
    }

    @Test
    public void getByUsername_userExists_shouldReturnUser() {
        var user = userRepository.getByUsername("pperic");
        assertNotNull(user);
        assertEquals("pperic", user.getUsername());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getByUsername_userDoesntExist_shouldThrow() {
        userRepository.getByUsername("divljiBill");
    }

    @Test
    public void getByEmail_userExists_shouldReturnUser() {
        var user = userRepository.getByEmail("pperic@fer.hr");
        assertNotNull(user);
        assertEquals("pperic@fer.hr", user.getEmail());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getByEmail_userDoesntExist_shouldThrow() {
        userRepository.getByEmail("divljiBill@fer.hr");
    }

    @Test
    public void deleteUser_userInDatabase_shouldDelete() {
        var user = userRepository.getOne(1L);
        userRepository.getByUsername(user.getUsername());
        assertNotNull(user);

        userRepository.delete(user.getId());
        try {
            userRepository.getOne(1L);
        } catch (EntityNotFoundException ex) {
            assertEquals(EntityNotFoundException.class, ex.getClass());
        }

        try {
            userRepository.getByUsername(user.getUsername());
        } catch (EntityNotFoundException ex) {
            assertEquals(EntityNotFoundException.class, ex.getClass());
        }

        try {
            userRepository.getByEmail(user.getEmail());
        } catch (EntityNotFoundException ex) {
            assertEquals(EntityNotFoundException.class, ex.getClass());
        }
    }
}
