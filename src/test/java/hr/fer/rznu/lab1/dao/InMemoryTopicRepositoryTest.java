package hr.fer.rznu.lab1.dao;

import hr.fer.rznu.lab1.dao.impl.InMemoryTopicRepository;
import hr.fer.rznu.lab1.model.Topic;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class InMemoryTopicRepositoryTest {

    private TopicRepository topicRepository;

    @Before
    public void initialize() throws NoSuchFieldException, IllegalAccessException {
        this.topicRepository = new InMemoryTopicRepository();

        var topic1 = new Topic().setId(1L).setName("Culture");
        var topic2 = new Topic().setId(2L).setName("Programming");
        var topic3 = new Topic().setId(1L).setName("Nature");

        Field db = topicRepository.getClass().getDeclaredField("db");
        Field userSequence = topicRepository.getClass().getDeclaredField("topicSequence");
        db.setAccessible(true);
        userSequence.setAccessible(true);

        HashMap<Long, Topic> realDb = (HashMap<Long, Topic>) db.get(topicRepository);
        realDb.put(1L, topic1);
        realDb.put(2L, topic2);
        realDb.put(3L, topic3);

        AtomicLong realUserSequence = (AtomicLong) userSequence.get(topicRepository);
        realUserSequence.set(4L);

        db.setAccessible(false);
        userSequence.setAccessible(false);
    }

    @Test
    public void getAll_usersPresent_shouldReturnList() {
        var allTopics = topicRepository.getAll();
        Assert.assertEquals(3, allTopics.size());
    }

}
