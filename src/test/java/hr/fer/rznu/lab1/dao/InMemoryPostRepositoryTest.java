package hr.fer.rznu.lab1.dao;

import hr.fer.rznu.lab1.dao.impl.InMemoryPostRepository;
import hr.fer.rznu.lab1.model.Post;
import hr.fer.rznu.lab1.model.Topic;
import hr.fer.rznu.lab1.model.User;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicLong;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

@RunWith(JUnit4.class)
public class InMemoryPostRepositoryTest {

    @InjectMocks
    private PostRepository postRepository;

    @Mock
    private UserRepository  userRepository;
    @Mock
    private TopicRepository topicRepository;

    @Before
    public void initialize() throws NoSuchFieldException, IllegalAccessException {
        this.postRepository = new InMemoryPostRepository();

        var post1 = new Post()
                .setId(1L)
                .setContent("This is some dummy content 1")
                .setTitle("This is title 1")
                .setCreatedBy(1L)
                .setTopic(1L);
        var post2 = new Post()
                .setId(2L)
                .setContent("This is some dummy content 2")
                .setTitle("This is title 2")
                .setCreatedBy(1L)
                .setTopic(1L);
        var post3 = new Post()
                .setId(1L)
                .setContent("This is some dummy content 3")
                .setTitle("This is title 3")
                .setCreatedBy(1L)
                .setTopic(1L);

        Field db = postRepository.getClass().getDeclaredField("db");
        Field postSequence = postRepository.getClass().getDeclaredField("postSequence");
        db.setAccessible(true);
        postSequence.setAccessible(true);

        HashMap<Long, Post> realDb = (HashMap<Long, Post>) db.get(postRepository);
        realDb.put(1L, post1);
        realDb.put(2L, post2);
        realDb.put(3L, post3);

        AtomicLong realUserSequence = (AtomicLong) postSequence.get(postRepository);
        realUserSequence.set(4L);

        db.setAccessible(false);
        postSequence.setAccessible(false);

        // Initialize mocks
        MockitoAnnotations.initMocks(this);

        var topic1 = new Topic().setId(1L).setName("Culture");
        var user1 = new User().setId(1L).setUsername("pperic").setEmail("pperic@fer.hr").setPassword("obscured");
        when(topicRepository.getOne(Matchers.eq(1L))).thenReturn(topic1);
        when(userRepository.getOne(Matchers.eq(1L))).thenReturn(user1);
    }

    @Test
    public void getAll_valuesPresent_shouldReturnList() {
        var posts = postRepository.getAll();
        System.out.println(posts);
        assertEquals(3, posts.size());
    }

    @Test
    public void getByUser_userExists_shouldReturnNonEmpty() {
        var posts = postRepository.getByUser(1L);
        System.out.println(posts);
        assertEquals(3, posts.size());
    }

}
