# SOC

First assignment for Service oriented computing at Faculty of Electrical engineering and computing, Zagreb


## Lab 1.
Goal of the first lab is to implement simple web service that exposes its functionalities using REST
API. Implement your own project or clone something popular (twitter, instagram, etc.). Your web
service must have:
1. REST interface *
2. documentation for the REST API
3. user authentication
4. automatic testing

You don't need fancy HTML interface. API endpoints should return JSON/XML content.
Web service must have at least two groups of resources. For instance, if you are developing simple
Instagram clone, you will have /users and /photos. Also, REST API must support nested
resources, e.g. /users/<user-id>/photos. You need to implement ALL CRUD HTTP methods for
each resource!

Please protect resources with some form of user authentication (cookies, sessions, basicauth, ...).
Documentation for the REST API is free-form text with action documented for each resource and
each method. Documentation can also be generated.

All API methods must be covered with tests. That means that you have to write very simple tests for
every resource and every method. Since there will not be many resources, this should not be a
problem. Exact form of tests is arbitrary and up to you. For testing purposes, please use testing tool
that comes built in your web framework of choice. If that is not an option, consider selenium or curl
(http://seleniumhq.org/, http://curl.haxx.se/).
Note: if you are using curl, then you must write script that will compare result you got from web
service with value you expect, e.g. in bash:
response = $(curl http://test-web-service.com/)
if [ $response -eq ... ] then ...

IMPORTANT: Use any programming language and web framework, just don't use some CMS tool.
Recommended setup is Python+Django+Django REST framework:
https://www.python.org/
https://www.djangoproject.com/
http://www.django-rest-framework.org/

This combination of tools enables easy REST API prototyping and comes with included testing
framework. It is also one of de facto industry standards for building REST web services.